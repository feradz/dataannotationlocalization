﻿using DataAnnotationLocalization.ViewModels.Member;
using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAnnotationLocalization.Controllers
{
    public class PersonalInfoController: Controller
    {
        [HttpGet]
        public ActionResult Edit()
        {
            PersonalInfo personalInfo = new PersonalInfo();
            personalInfo.Name = "Default Name";
            return View(personalInfo);
        }

        [HttpPost]
        public ActionResult Edit(PersonalInfo personalInfo)
        {
            if (!ModelState.IsValid)
            {
                return View(personalInfo);
            }
            personalInfo = new PersonalInfo();
            personalInfo.Name = "Default Name Edit";
            return View(personalInfo);
        }
    }
}
