﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataAnnotationLocalization.ViewModels.Member
{
    public class PersonalInfo
    {
        [Display(Name = "NameDisplay", ResourceType = typeof(DataAnnotationLocalization.ViewModels.Member.PersonalInfo))]
        [Required(ErrorMessageResourceName = "NameRequired", ErrorMessageResourceType = typeof(DataAnnotationLocalization.ViewModels.Member.PersonalInfo))]
        public string Name { get; set; }
    }
}
